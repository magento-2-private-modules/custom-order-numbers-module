<?php

namespace MacPain\CustomOrderNumbers\Model\SalesSequence;

class GetMetaStoreId
{

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $resourceConnection;

    /**
     * @param \Magento\Framework\App\ResourceConnection $resourceConnection
     */
    public function __construct(\Magento\Framework\App\ResourceConnection $resourceConnection)
    {
        $this->resourceConnection = $resourceConnection;
    }

    /**
     * @param int $metaId
     * @return int
     */
    public function execute(int $metaId)
    {
        $connection = $this->getConnection();
        $table = $this->getConnection()->getTableName('sales_sequence_meta');
        $bind = ['meta_id' => $metaId];

        $select = $connection->select()->from(
            $table,
            ['store_id']
        )->where(
            'meta_id = :meta_id'
        );
        $storeId = $connection->fetchOne($select, $bind);

        if ($storeId) {
            return $storeId;
        }

        return 0;
    }

    /**
     * @return \Magento\Framework\DB\Adapter\AdapterInterface
     */
    protected function getConnection()
    {
        return $this->resourceConnection->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);
    }

}
