<?php

namespace MacPain\CustomOrderNumbers\Model\SalesSequence;

class GetMetaEntityType
{

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $resourceConnection;

    /**
     * @param \Magento\Framework\App\ResourceConnection $resourceConnection
     */
    public function __construct(\Magento\Framework\App\ResourceConnection $resourceConnection)
    {
        $this->resourceConnection = $resourceConnection;
    }

    /**
     * @param \Magento\SalesSequence\Model\Profile $profile
     * @return string
     */
    public function execute(int $metaId)
    {
        $connection = $this->getConnection();
        $table = $this->getConnection()->getTableName('sales_sequence_meta');
        $bind = ['meta_id' => $metaId];

        $select = $connection->select()->from(
            $table,
            ['entity_type']
        )->where(
            'meta_id = :meta_id'
        );
        $entityType = $connection->fetchOne($select, $bind);

        if ($entityType) {
            return $entityType;
        }

        return '';
    }

    /**
     * @return \Magento\Framework\DB\Adapter\AdapterInterface
     */
    protected function getConnection()
    {
        return $this->resourceConnection->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);
    }

}
