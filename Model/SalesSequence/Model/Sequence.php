<?php

namespace MacPain\CustomOrderNumbers\Model\SalesSequence\Model;

class Sequence extends \Magento\SalesSequence\Model\Sequence
{

    protected \MacPain\CustomOrderNumbers\Model\SalesSequence\GetMetaEntityType $getMetaEntityType;
    protected \MacPain\CustomOrderNumbers\Model\SalesSequence\GetMetaStoreId $getMetaStoreId;
    protected \MacPain\CustomOrderNumbers\Helper\SystemConfiguration $systemConfiguration;
    protected \Magento\SalesSequence\Model\Meta $meta;

    public function __construct(
        \MacPain\CustomOrderNumbers\Model\SalesSequence\GetMetaEntityType $getMetaEntityType,
        \MacPain\CustomOrderNumbers\Model\SalesSequence\GetMetaStoreId $getMetaStoreId,
        \MacPain\CustomOrderNumbers\Helper\SystemConfiguration $systemConfiguration,
        \Magento\SalesSequence\Model\Meta $meta,
        \Magento\Framework\App\ResourceConnection $resource,
        $pattern = \Magento\SalesSequence\Model\Sequence::DEFAULT_PATTERN
    )
    {
        $this->getMetaEntityType = $getMetaEntityType;
        $this->getMetaStoreId = $getMetaStoreId;
        $this->systemConfiguration = $systemConfiguration;
        $this->meta = $meta;

        $pattern = $this->preparePattern($pattern);

        parent::__construct($meta, $resource, $pattern);
    }

    /**
     * @param string $pattern
     * @return string
     */
    protected function preparePattern(string $pattern): string
    {
        $isEnabled = $this->systemConfiguration->isEnabled();
        if (!$isEnabled) {
            return $pattern;
        }

        $digitsNumber = 9;
        $entityType = $this->getMetaEntityType->execute($this->meta->getId());
        switch ($entityType) {
            case \MacPain\CustomOrderNumbers\Helper\Constants::ORDER_ENTITY_TYPE:
                $digitsNumber = $this->systemConfiguration->getOrderDigitsNumber();
                break;

            case \MacPain\CustomOrderNumbers\Helper\Constants::INVOICE_ENTITY_TYPE:
                $digitsNumber = $this->systemConfiguration->getInvoiceDigitsNumber();
                break;

            case \MacPain\CustomOrderNumbers\Helper\Constants::CREDITMEMO_ENTITY_TYPE:
                $digitsNumber = $this->systemConfiguration->getCreditmemoDigitsNumber();
                break;

            case \MacPain\CustomOrderNumbers\Helper\Constants::SHIPMENT_ENTITY_TYPE:
                $digitsNumber = $this->systemConfiguration->getShipmentDigitsNumber();
                break;
        }

        $digitsNumber = intval($digitsNumber);
        if (!is_int($digitsNumber)) {
            return $pattern;
        }

        if ($digitsNumber < 1) {
            return $pattern;
        }

        return "%s%'.0" . $digitsNumber . "d%s";
    }

}
