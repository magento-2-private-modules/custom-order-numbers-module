<?php

namespace MacPain\CustomOrderNumbers\Helper;

class SystemConfiguration
{

    protected \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig;

    /**
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return (bool) $this->scopeConfig->getValue(
            \MacPain\CustomOrderNumbers\Helper\Constants::IS_ENABLED_PATH,
            \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITE
        );
    }

    /**
     * @return string
     */
    public function getOrderPrefixFormat(): string
    {
        return (string) $this->scopeConfig->getValue(
            \MacPain\CustomOrderNumbers\Helper\Constants::ORDER_PREFIX_FORMAT_PATH,
            \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITE
        );
    }

    /**
     * @return string
     */
    public function getInvoicePrefixFormat(): string
    {
        return (string) $this->scopeConfig->getValue(
            \MacPain\CustomOrderNumbers\Helper\Constants::INVOICE_PREFIX_FORMAT_PATH,
            \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITE
        );
    }

    /**
     * @return string
     */
    public function getCreditmemoPrefixFormat(): string
    {
        return (string) $this->scopeConfig->getValue(
            \MacPain\CustomOrderNumbers\Helper\Constants::CREDITMEMO_PREFIX_FORMAT_PATH,
            \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITE
        );
    }

    /**
     * @return string
     */
    public function getShipmentPrefixFormat(): string
    {
        return (string) $this->scopeConfig->getValue(
            \MacPain\CustomOrderNumbers\Helper\Constants::SHIPMENT_PREFIX_FORMAT_PATH,
            \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITE
        );
    }

    /**
     * @return string
     */
    public function getOrderSuffixFormat(): string
    {
        return (string) $this->scopeConfig->getValue(
            \MacPain\CustomOrderNumbers\Helper\Constants::ORDER_SUFFIX_FORMAT_PATH,
            \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITE
        );
    }

    /**
     * @return string
     */
    public function getInvoiceSuffixFormat(): string
    {
        return (string) $this->scopeConfig->getValue(
            \MacPain\CustomOrderNumbers\Helper\Constants::INVOICE_SUFFIX_FORMAT_PATH,
            \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITE
        );
    }

    /**
     * @return string
     */
    public function getCreditmemoSuffixFormat(): string
    {
        return (string) $this->scopeConfig->getValue(
            \MacPain\CustomOrderNumbers\Helper\Constants::CREDITMEMO_SUFFIX_FORMAT_PATH,
            \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITE
        );
    }

    /**
     * @return string
     */
    public function getShipmentSuffixFormat(): string
    {
        return (string) $this->scopeConfig->getValue(
            \MacPain\CustomOrderNumbers\Helper\Constants::SHIPMENT_SUFFIX_FORMAT_PATH,
            \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITE
        );
    }

    /**
     * @return string
     */
    public function getOrderDigitsNumber(): string
    {
        return (string) $this->scopeConfig->getValue(
            \MacPain\CustomOrderNumbers\Helper\Constants::ORDER_DIGITS_NUMBER_INCREMENT_PATH,
            \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITE
        );
    }

    /**
     * @return string
     */
    public function getInvoiceDigitsNumber(): string
    {
        return (string) $this->scopeConfig->getValue(
            \MacPain\CustomOrderNumbers\Helper\Constants::INVOICE_DIGITS_NUMBER_INCREMENT_PATH,
            \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITE
        );
    }

    /**
     * @return string
     */
    public function getCreditmemoDigitsNumber(): string
    {
        return (string) $this->scopeConfig->getValue(
            \MacPain\CustomOrderNumbers\Helper\Constants::CREDITMEMO_DIGITS_NUMBER_INCREMENT_PATH,
            \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITE
        );
    }

    /**
     * @return string
     */
    public function getShipmentDigitsNumber(): string
    {
        return (string) $this->scopeConfig->getValue(
            \MacPain\CustomOrderNumbers\Helper\Constants::SHIPMENT_DIGITS_NUMBER_INCREMENT_PATH,
            \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITE
        );
    }

}
