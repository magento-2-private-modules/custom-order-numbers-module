<?php

namespace MacPain\CustomOrderNumbers\Helper;

use Magento\Framework\App\Helper\Context;

class FormatString extends \Magento\Framework\App\Helper\AbstractHelper
{

    protected \Magento\Store\Model\StoreManagerInterface $storeManager;
    protected \Magento\Directory\Model\CountryFactory $countryFactory;

    /**
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Directory\Model\CountryFactory $countryFactory
     * @param \Magento\Framework\App\Helper\Context $context
     */
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Directory\Model\CountryFactory $countryFactory,
        \Magento\Framework\App\Helper\Context $context
    )
    {
        $this->storeManager = $storeManager;
        $this->countryFactory = $countryFactory;

        parent::__construct($context);
    }

    /**
     * @param string $str
     * @param int $storeId
     * @return string
     */
    public function execute(string $str, int $storeId): string
    {
        $storeCode = $this->prepareStoreCode($storeId);
        $countryCode = $this->prepareCountryCode();
        $currencyCode = $this->prepareCurrencyCode($storeId);
        $dates = $this->prepareDates();

        $str = str_replace("[store_id]", $storeId, $str);
        $str = str_replace("[store_code]", $storeCode, $str);
        $str = str_replace("[country_code]", $countryCode, $str);
        $str = str_replace("[currency_code]", $currencyCode, $str);
        $str = str_replace("[yyyy]", $dates['yyyy'], $str);
        $str = str_replace("[yy]", $dates['yy'], $str);
        $str = str_replace("[mm]", $dates['mm'], $str);
        $str = str_replace("[m]", $dates['m'], $str);
        $str = str_replace("[dd]", $dates['dd'], $str);
        $str = str_replace("[d]", $dates['d'], $str);

        return str_replace(" ", "", $str);
    }

    /**
     * @param int $storeId
     * @return string
     */
    protected function prepareStoreCode(int $storeId): string
    {
        $storeCode = 'default';
        try {
            $storeCode = $this->storeManager->getStore($storeId)->getCode();
        } catch (\Magento\Framework\Exception\NoSuchEntityException $noSuchEntityException) {
            $this->_logger->error($noSuchEntityException->getMessage());
        }

        return strtoupper($storeCode);
    }

    /**
     * @return string
     */
    protected function prepareCountryCode(): string
    {
        return $this->scopeConfig->getValue('general/country/default');
    }

    /**
     * @param int $storeId
     * @return string
     */
    protected function prepareCurrencyCode(int $storeId): string
    {
        try {
            return $this->storeManager->getStore($storeId)->getCurrentCurrencyCode();
        } catch (\Magento\Framework\Exception\NoSuchEntityException $noSuchEntityException) {
            $this->_logger->error($noSuchEntityException->getMessage());
        }

        return 'USD';
    }

    /**
     * @return array
     */
    protected function prepareDates(): array
    {
        $date = new \DateTime();

        return [
            'yyyy' => $date->format('Y'),
            'yy' => $date->format('y'),
            'mm' => $date->format('m'),
            'm' => $date->format('n'),
            'dd' => $date->format('d'),
            'd' => $date->format('j'),
        ];
    }

}
