<?php

namespace MacPain\CustomOrderNumbers\Helper;

class Constants
{

    const ORDER_ENTITY_TYPE = 'order';
    const INVOICE_ENTITY_TYPE = 'invoice';
    const CREDITMEMO_ENTITY_TYPE = 'creditmemo';
    const SHIPMENT_ENTITY_TYPE = 'shipment';

    const IS_ENABLED_PATH = 'custom_order_numbers/general_configuration/enable';
    const ORDER_PREFIX_FORMAT_PATH = 'custom_order_numbers/order/prefix_format';
    const INVOICE_PREFIX_FORMAT_PATH = 'custom_order_numbers/invoice/prefix_format';
    const CREDITMEMO_PREFIX_FORMAT_PATH = 'custom_order_numbers/creditmemo/prefix_format';
    const SHIPMENT_PREFIX_FORMAT_PATH = 'custom_order_numbers/shipment/prefix_format';

    const ORDER_SUFFIX_FORMAT_PATH = 'custom_order_numbers/order/suffix_format';
    const INVOICE_SUFFIX_FORMAT_PATH = 'custom_order_numbers/invoice/suffix_format';
    const CREDITMEMO_SUFFIX_FORMAT_PATH = 'custom_order_numbers/creditmemo/suffix_format';
    const SHIPMENT_SUFFIX_FORMAT_PATH = 'custom_order_numbers/shipment/suffix_format';

    const ORDER_DIGITS_NUMBER_INCREMENT_PATH = 'custom_order_numbers/order/digits_number';
    const INVOICE_DIGITS_NUMBER_INCREMENT_PATH = 'custom_order_numbers/invoice/digits_number';
    const CREDITMEMO_DIGITS_NUMBER_INCREMENT_PATH = 'custom_order_numbers/creditmemo/digits_number';
    const SHIPMENT_DIGITS_NUMBER_INCREMENT_PATH = 'custom_order_numbers/shipment/digits_number';

}
