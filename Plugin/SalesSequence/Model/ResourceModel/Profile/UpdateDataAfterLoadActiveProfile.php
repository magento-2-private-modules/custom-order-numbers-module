<?php

namespace MacPain\CustomOrderNumbers\Plugin\SalesSequence\Model\ResourceModel\Profile;

class UpdateDataAfterLoadActiveProfile
{

    protected \MacPain\CustomOrderNumbers\Model\SalesSequence\GetMetaEntityType $getMetaEntityType;
    protected \MacPain\CustomOrderNumbers\Model\SalesSequence\GetMetaStoreId $getMetaStoreId;
    protected \MacPain\CustomOrderNumbers\Helper\SystemConfiguration $systemConfiguration;
    protected \MacPain\CustomOrderNumbers\Helper\FormatString $formatString;

    public function __construct(
        \MacPain\CustomOrderNumbers\Model\SalesSequence\GetMetaEntityType $getMetaEntityType,
        \MacPain\CustomOrderNumbers\Model\SalesSequence\GetMetaStoreId $getMetaStoreId,
        \MacPain\CustomOrderNumbers\Helper\SystemConfiguration $systemConfiguration,
        \MacPain\CustomOrderNumbers\Helper\FormatString $formatString
    )
    {
        $this->getMetaEntityType = $getMetaEntityType;
        $this->getMetaStoreId = $getMetaStoreId;
        $this->systemConfiguration = $systemConfiguration;
        $this->formatString = $formatString;
    }

    /**
     * @param \Magento\SalesSequence\Model\ResourceModel\Profile $subject
     * @param \Magento\SalesSequence\Model\Profile $result
     * @param int $metadataId
     * @return \Magento\SalesSequence\Model\Profile
     */
    public function afterLoadActiveProfile(
        \Magento\SalesSequence\Model\ResourceModel\Profile $subject,
        \Magento\SalesSequence\Model\Profile $result,
        int $metadataId
    ): \Magento\SalesSequence\Model\Profile
    {
        $isEnabled = $this->systemConfiguration->isEnabled();
        if (!$isEnabled) {
            return $result;
        }

        $storeId = $this->getMetaStoreId->execute($metadataId);
        $entityType = $this->getMetaEntityType->execute($metadataId);
        switch ($entityType) {
            case \MacPain\CustomOrderNumbers\Helper\Constants::ORDER_ENTITY_TYPE:

                $prefixFormat = $this->systemConfiguration->getOrderPrefixFormat();
                $prefix = $this->formatString->execute($prefixFormat, $storeId);
                $result->setData('prefix', $prefix);

                $suffixFormat = $this->systemConfiguration->getOrderSuffixFormat();
                $suffix = $this->formatString->execute($suffixFormat, $storeId);
                $result->setData('suffix', $suffix);

                break;
            case \MacPain\CustomOrderNumbers\Helper\Constants::INVOICE_ENTITY_TYPE:

                $prefixFormat = $this->systemConfiguration->getInvoicePrefixFormat();
                $prefix = $this->formatString->execute($prefixFormat, $storeId);
                $result->setData('prefix', $prefix);

                $suffixFormat = $this->systemConfiguration->getInvoiceSuffixFormat();
                $suffix = $this->formatString->execute($suffixFormat, $storeId);
                $result->setData('suffix', $suffix);

                break;
            case \MacPain\CustomOrderNumbers\Helper\Constants::CREDITMEMO_ENTITY_TYPE:

                $prefixFormat = $this->systemConfiguration->getCreditmemoPrefixFormat();
                $prefix = $this->formatString->execute($prefixFormat, $storeId);
                $result->setData('prefix', $prefix);

                $suffixFormat = $this->systemConfiguration->getCreditmemoSuffixFormat();
                $suffix = $this->formatString->execute($suffixFormat, $storeId);
                $result->setData('suffix', $suffix);

                break;
            case \MacPain\CustomOrderNumbers\Helper\Constants::SHIPMENT_ENTITY_TYPE:

                $prefixFormat = $this->systemConfiguration->getShipmentPrefixFormat();
                $prefix = $this->formatString->execute($prefixFormat, $storeId);
                $result->setData('prefix', $prefix);

                $suffixFormat = $this->systemConfiguration->getShipmentSuffixFormat();
                $suffix = $this->formatString->execute($suffixFormat, $storeId);
                $result->setData('suffix', $suffix);

                break;
        }

        return $result;
    }

}
