<?php

namespace MacPain\CustomOrderNumbers\Controller\Index;

class Index implements \Magento\Framework\App\Action\HttpGetActionInterface
{

    protected \Magento\Store\Model\StoreManagerInterface $storeManager;
    protected \MacPain\CustomOrderNumbers\Helper\SystemConfiguration $systemConfiguration;
    protected \MacPain\CustomOrderNumbers\Helper\FormatString $formatString;

    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \MacPain\CustomOrderNumbers\Helper\SystemConfiguration $systemConfiguration,
        \MacPain\CustomOrderNumbers\Helper\FormatString $formatString
    )
    {
        $this->storeManager = $storeManager;
        $this->systemConfiguration = $systemConfiguration;
        $this->formatString = $formatString;
    }

    public function execute()
    {
        $storeId = $this->storeManager->getStore()->getId();
        $orderPrefix = $this->systemConfiguration->getOrderPrefixFormat();
        echo $this->formatString->execute($orderPrefix, $storeId);
        echo "<br>";
        $invoicePrefix = $this->systemConfiguration->getInvoicePrefixFormat();
        echo $this->formatString->execute($invoicePrefix, $storeId);
        echo "<br>";
        $creditmemoPrefix = $this->systemConfiguration->getCreditmemoPrefixFormat();
        echo $this->formatString->execute($creditmemoPrefix, $storeId);
        echo "<br>";
        $shipmentPrefix = $this->systemConfiguration->getShipmentPrefixFormat();
        echo $this->formatString->execute($shipmentPrefix, $storeId);
        exit;
    }

}
